MiniX5 readme 


screenshot:


![](https://NadjaNaneva.gitlab.io/aep/MiniX5/MiniX5billede.png)


link til miniX5: https://nadjananeva.gitlab.io/aep/MiniX5/index.html


link til source code: https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX5/miniX5.js


Beskrivelse af design


Jeg har skabt en random number generator, fordi jeg synes at det er den form for generator jeg bruger mest og ikke kun til matematik. Hvad man kan se på MiniX'en er en knap der generer et nummer og en overskrift. Hvis et tal er genereret kan man se en anden knap komme frem som gør man kan fortsætte med at genere tal. Dette er forklaret mere dybdegående i kode afsnittet. Dog har mit program det at den viser komma og ikke hele tal. Jeg mener også at den ekstra krævede interaktion for at generer et andet tal kan være trættende over tid og ville ønske at forbedre det.


Kode


Jeg har nu tænkt mig at gennemgå nogle af de sværeste og vigtige kodestykker i mit produkt:


let knapagian;
let knapgenerate;
let number;


her skaber jeg 3 variabler men giver dem ingen værdi. Det sker senere ved for eksempel dette her linjestykke hvor jeg siger at number skal være ligmed et tilfældigt tal mellem 0 og 10:


number = random(0, 10);


senere laver jeg også en if-statement som der kræves for at få systemet til at fungere
 
if (mouseButton) {
 knapgenerate.mousePressed (text(number, 300, 280))
  knapagian = createButton("another number?");
 knapagian.position(280, 500);
 knapagian.size(150, 70);
knapagian.style("background-color", "#E5694F");
knapagian.style("color", "#ffffff");
knapagian.mousePressed (refreshPage)
}


hvad der sker her at hvis musen interagerer med knappen, vil der komme et tilfældigt tal ( knapgenerate.mousePressed (text(number, 300, 280))) og en anden knap “  knapagian = createButton("another number?")”. Hvad der også står er at hvis den nye knap er trykket på skal den nedstående funktion ske:


function refreshPage () {
window.location.reload(
 knapgenerate.mousePressed (text(number, 300, 280)))
//for loop
for (let i = 0; i < 10; i++) {
console.log(number)}
}


hvad function refreshPage siger der skal ske er windows skal reloade og der kun skal være generer knappen. Derudover er der et for loop som siger hjælper med at skrive tallet ind i console.log. Dog har jeg opdaget en fejl jeg gerne vil ønske at ændre på og det er at det ikke er hvert genererede tal der bliver skrevet ind, men det nuværende. Jeg kunne godt tænke mig at det var alle genererede tal, så hvis I i feedbacken kan hjælpe eller se på det ville jeg være glad.


Refleksion angående læsning og temaet


Jeg kan komme i tanke om en form for generende system der for nyligt fik meget opmærksomhed og omtale var billede AI generatorer. Det er et system der enten taget et af dine billeder og generer det i en specifik stil (af dit ønske fx. Picasso, Da Vinci…) eller bare giver et billede efter dit ønske. Problemet der var at man fandt ud af noget af det var stjålet kunst eller beskyldte kunstnere indenfor den tegnestil for tyveri og ikke at skabe værket dem selv. Mens jeg i starten synes det var spændende at bruge, mente jeg det var for etisk ukorrekt at fortsætte med det. Dog bruger jeg ligesom mit eget produkt en random number generator tit. 


Angående læsningen til denne uge ville jeg også mene spil ville være svære at nyde uden chancen for noget tilfældigt. Jeg havde ikke gjort mig så mange overvejelser på temaet siden det ikke har været synligt eller som bruger relevant at kende til. Hvad jeg fik ud af det er selv noget så simpelt og kort beskriveligt som min MiniX 4 kan analyseres af fx. de former for randomness der var i texten. 
