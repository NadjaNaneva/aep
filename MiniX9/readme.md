# Group members
[Nadja] (this gitlab)
<br>
[Kristín](https://gitlab.com/kristintrang/aesthetic-programming/-/tree/main)
<br>
[Salomon](https://gitlab.com/salomonsimonsen8/aesthetic-programming/-/tree/main)
<br>
[Marta] (https://gitlab.com/martatausen/aesthetic-programmering/-/tree/main)
<br>

# The Tech Advocate - description
![]() <img src="/minix9/screen.png" width="600">
<br>
Link to: [Program](https://martatausen.gitlab.io/aesthetic-programmering/minix9/index.html)
<br>
Link to: [Source Code](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix9/minix9.js)
<br>
Link to: [JSON file](https://gitlab.com/martatausen/aesthetic-programmering/-/blob/main/minix9/statements.json)
<br>
# DESCRIPTION - The Tech Advocate
For this MiniX the topic was E-Lit which we were given examples of in class, such as “My boyfriend came back from war” by Olia Lialina or the one we worked with in the textbook called “Vocable Code” by Winnie Soon.
<br>

While having the option of doing a more poetic story, we in the group chose to go a more socially critical route and created this project called “The Tech Advocate”. We decided the best route was not to be too subtle.
<br>

“The Tech Advocate” is an example of how big companies use language to market themselves and manipulate users, while in actuality, through their actions, doing something else. The companies used here are Meta, Twitter, TikTok, Amazon, and Google.
<br>

For every company we wanted to focus on different issues. For example, with Meta we focused on user privacy (with a political twist to it) and with Amazon we focused on the workplace and their treatment of their employees. We wanted it to be diverse, in regards to different topics, even though some of these companies deal with the same controversies, like TikTok and Meta with its inappropriate content on their platform.
<br>

Visually it is very simple. You see bubbles with statements from said companies and when the mouse is hovering over a bubble you receive information and facts about what the company's actions really are, often contradicting the previous sweet words. The aesthetics of the program is to further push the narrative of the irony of what is really said and then what is actually done. Hence, the cute and bubbly feeling of the program before you hover over the bubble, and then the radical change to the dark and ugly truth.

# The Code 
Here is a tidbit from the code how the hovering functions within our program we can look at this code example:

```
function meta() {
 
    //Statement 1
    if (mouseX > 90 && mouseX < 420 && mouseY > 320 && mouseY < 445) {
        background(0);
        fill(expose, 0, 0);
        textAlign(CORNER);
        text(actualFact, 80, 200, 1200, 600);
    } else {
        fill(0);
        textAlign(CENTER);
        text(companyValue, 250, 400);
    }
```
What this code shows is how we hardcoded the background switching when the mouse is over with a simple if statement.
<br>

What it says is that if mouseX is between 90 pixels and 420 pixels and mouseY is between 320 and 445 it will show “the truth” about a company else it will show what the company is commercializing. Every bubble is hardcoded in this method. 
<br>

Another important part of this project is where we have the statements stored. Which is all stored in a JSON file. 
<br>
What we learned about JSON is that it is used by performing 4 simple steps. 
<br>

**Step one:** create a .json file
<br>
**Step two:** load your .json file in the sketch file with a function preload ()
<br>
**Step three:** Refer to the data-bid you want to use
<br>
**Step four:** Use it

In the function preload we refer to the .json file for step one and our step two like this:

```
function preload() {
    statements = loadJSON("statements.json");
    facade = loadImage("facade.png");
    exterior = loadFont("Poppins-Regular.ttf");
}
```

Here we have carefully chosen to name our variables something relevant to the overall theme of the program and giving meaning to them, so you can almost read the code itself as a poetic text, this is especially apparent when the statements from the json files are being loaded in:
```
function meta() {
    let privacy = 0;
    let companyValue = statements.companies[privacy].value;
    let actualFact = statements.companies[privacy].fact;
```

Even in the code here itself the irony of Meta’s value “The Future is Private” comes into play in the fact that our variable that loads in the Meta statements is 0. There are other little tidbits like this to be found in the code itself.


# Reflection
We were largely inspired by Shoshana Zuboff’s Surveillance Capitalism, where in the VPRO documentary Zuboff sheds light on big tech’s public operations and shadow operations. In the same vein, our program has a bubbly look as the “public operations”, whilst when hovering over the bubbles you’ll have the “shadow operations” shown.
<br>
We believe our project is a critical design given the fact it challenges the ethics and ways-of-operation within certain companies that dominate user platforms.  
<br>
If a user views our E-Lit work they might be made to feel uncomfortable or displeased with the way the current tech companies function and while not giving a solution, might spark awareness and might wish to change it in whatever capacity possible giving an example of how code can influence people. 
<br>

The code itself also isn’t written in a problem solving lens, as in the naming and framing in the code isn't done in ways that would be easy to understand for programmers, however our code rather comes under E-literature where the code itself has poetry qualities, the stuff shown on the screen itself is a lot more concrete and out there, while the code itself tells the same story, but in a different way for example the code that creates the "public operation" bubbles looks like this: `function createStatement(inaccurate, statement)` the naming is purposefully done to emphasis our point of our work in the code itself. As Kathrine Hayles states, “that code run on machine is performative in a much stronger sense than that attributed to language” is especially true for our work. The point we want reflected is much stronger when the visual aspects of it are also shown.
<br>

All of the "shadow operation" text is based on real life articles about the different issues the companies have and all the references we have used can be found in the reference list. Speaking in regards to code influencing the real world, it's interesting to look at for example Meta, with how it is programmed to take a lot more data then we're aware of and using it in insidious ways that cause legal and moral issues, which really is the root of the many scandals Meta has had over the years. This is all very connected to the topic of data capture and how data circulates within society and company borders. 

If we wanted to look at it from the perspective in regards to the Geoff Cox and Alex McLean text “Vocable Code” project we read about this week, our code is not written to problem solve, but rather to raise awareness as a tool to give voice to users and perhaps to create a wish create a dialogue between them and the said companies. Of course more could be added, but these are major companies with great influence - thus giving us reason to base our project on them. 


# Reference list
## From class
Soon Winnie & Cox, Geoff, Aesthetic Programming: A Handbook of Software Studies, London: Open Humanities Press, 2020
<br/>
Lialina Olia, (1996) My Boyfriend Came Back from the War

## Meta
Hern, A. (2022, August 15). Meta injecting code into websites to track its users, research says. The Guardian. [link to article](https://www.theguardian.com/technology/2022/aug/11/meta-injecting-code-into-websites-visited-by-its-users-to-track-them-research-says)
<br>
<br>
Facebook hit with four lawsuits in one week over Cambridge Analytica scandal - Business & Human Rights Resource Centre. (n.d.). Business & Human Rights Resource Centre. [link to article](https://www.business-humanrights.org/en/latest-news/facebook-hit-with-four-lawsuits-in-one-week-over-cambridge-analytica-scandal/)
<br/>
<br>
McCallum, B. S. (2022, December 23). Meta settles Cambridge Analytica scandal case for $725m. BBC News. [link to article](https://www.bbc.com/news/technology-64075067)
<br/>
<br>
Rosenberg, M., Confessore, N., & Cadwalladr, C. (2019, March 19). How Trump Consultants Exploited the Facebook Data of Millions. The New York Times. [link to article](https://www.nytimes.com/2018/03/17/us/politics/cambridge-analytica-trump-campaign.html)
<br/>
<br>
Meisenzahl, M., & Canales, K. (2021, November 3). The 16 biggest scandals Mark Zuckerberg faced over the last decade as he became one of the world’s most powerful people. Business Insider. [link to article](https://www.businessinsider.com/mark-zuckerberg-scandals-last-decade-while-running-facebook-2019-12?r=US&IR=T#3-the-company-faced-heavy-criticisms-for-misinformation-surrounding-the-2016-us-presidential-election-especially-after-a-buzzfeed-report-showed-that-false-news-stories-outperformed-real-news-mark-zuckerberg-posted-to-facebook-an-apology-and-said-the-company-plans-to-improve-3)
<br/>
<br>
Amnesty International. (2023). Myanmar: Facebook’s systems promoted violence against Rohingya; Meta owes reparations – new report.[link to article](https://www.amnesty.org/en/latest/news/2022/09/myanmar-facebooks-systems-promoted-violence-against-rohingya-meta-owes-reparations-new-report/)
<br>
<br>
Facebook, Google and Twitter’s political ad policies are bad for democracy. (2019, December 18). CNN. [link to article](https://edition.cnn.com/2019/12/18/perspectives/facebook-google-twitter-political-ads/index.html)

## Twitter
Twitter about page [link](https://about.twitter.com/en)
<br>
<br/>
Polumbo, B. (2023, April 14). Elon Musk Promised Free Speech on Twitter. He’s Betrayed It Again and Again | Opinion. Newsweek. [link to article](https://www.newsweek.com/elon-musk-promised-free-speech-twitter-hes-betrayed-it-again-again-opinion-1794478)
<br/>
<br>
Kohli, A. (2023, January 21). Elon Musk Takes Stand to Defend Controversial Tweets. Here’s What to Know About the Trial So Far. Time. [link to article](https://time.com/6249127/elon-musk-on-trial-for-tesla-tweets/)
<br/>
<br>
Paris, M. (2022, December 18). Elon Musk Bans Tech Journalists On Twitter As More Flee To Mastodon: Here’s Who To Follow. Forbes. [link to article](https://www.forbes.com/sites/martineparis/2022/12/17/elon-musk-bans-journalists-on-twitter-as-more-flee-to-mastodon-heres-who-to-follow/?sh=5eb2c24c302c)

## TikTok
Hadero, H. (2023, March 1). Why TikTok is being banned on gov’t phones in US and beyond. AP NEWS. [link to article](https://apnews.com/article/why-is-tiktok-being-banned-7d2de01d3ac5ab2b8ec2239dc7f2b20d)
<br/>
<br>
Reporter, G. S. (2023, March 25). The TikTok wars – why the US and China are feuding over the app. The Guardian. [link to article](https://www.theguardian.com/technology/2023/mar/16/the-tiktok-wars-why-the-us-and-china-are-feuding-over-the-app)
<br>
<br>
Fung, B. (2023, March 24). TikTok collects a lot of data. But that’s not the main reason officials say it’s a security risk. CNN. [link to article](https://edition.cnn.com/2023/03/24/tech/tiktok-ban-national-security-hearing/index.html)
<br/>
<br>
Hern, A. (2022a, July 20). TechScape: suspicious of TikTok? You’re not alone. The Guardian. [link to article](https://www.theguardian.com/technology/2022/jul/20/tiktoks-privacy-problem-isnt-what-you-think)
<br>
<br>
Tidy, B. J. (2023, February 28). TikTok answers three big cyber-security fears about the app. BBC News. [link to article](https://www.bbc.com/news/technology-64797355)
<br>
<br>
Gilbert, D. (2023, March 21). TikTok Is Pushing Incel and Suicide Videos to 13-Year-Olds. Vice News. [link to article](https://www.vice.com/en/article/qjv4jw/tiktok-incels-targeting-young-users)
<br>
<br>
Fitz-Gibbon, J. (2023, March 23). Parents of LI suicide teen break down at TikTok hearing on Capitol Hill. New York Post. [link to article](https://nypost.com/2023/03/23/parents-of-li-suicide-teen-break-down-during-tiktok-hearins-on-capitol-hill/)
## Amazon
Thorbecke, C. (2023, January 18). US Labor Department accuses Amazon of failing to keep warehouse workers safe. CNN. [link to article](https://edition.cnn.com/2023/01/18/tech/amazon-osha-citation/index.html)
<br/>
<br>
Palmer, A. (2023, January 18). Amazon cited by Labor Department for exposing warehouse workers to safety hazards. CNBC. [link to article](https://www.cnbc.com/2023/01/18/amazon-cited-by-osha-for-exposing-warehouse-workers-to-safety-hazards.html)
<br>
<br>
PCMag. (2021, October 29). Amazon fires 2 workers who publicly criticized warehouse conditions. Mashable. [link to article](https://mashable.com/article/amazon-fires-workers-warehouse-conditions)
<br>
<br>
Dawood, S. (2023, February 13). Amazon’s worker surveillance “leads to extreme stress and anxiety.” New Statesman. [link to article](https://www.newstatesman.com/spotlight/cybersecurity/2023/02/amazon-workers-staff-surveillance-extreme-stress-anxiety)
<br>
<br>
Anderson, G. (2022, October 17). Controversy Persists Over Amazon’s Use Of Data To Favor Its Knockoffs. Forbes. [link to article](https://www.forbes.com/sites/retailwire/2022/10/17/controversy-persists-over-amazons-use-of-data-to-favor-its-knockoffs/?sh=a909c18f64eb)

## Google
Press corner. (2019). European Commission - European Commission. [link to press release](https://ec.europa.eu/commission/presscorner/detail/en/IP_19_1770)
<br/>
<br>
Press corner. (2017). European Commission - European Commission. [link to press release](https://ec.europa.eu/commission/presscorner/detail/es/MEMO_17_1785)
<br>
<br>
Justice Department Sues Google for Monopolizing Digital Advertising. (2023, February 2). [link to DoJ](https://www.justice.gov/opa/pr/justice-department-sues-google-monopolizing-digital-advertising-technologies)
<br>
<br>
Allyn, B. (2023, January 25). U.S. files second antitrust suit against Google’s ad empire, seeks to break it up. NPR. [link to article](https://www.npr.org/2023/01/24/1151055903/doj-files-second-antitrust-suit-against-google-seeks-to-break-up-its-ad-business)
<br>
<br>
Ghaffary, S. (2020, December 9). Why Timnit Gebru’s controversial Google exit is setting off a firestorm in tech. Vox. [link to article](https://www.vox.com/recode/2020/12/4/22153786/google-timnit-gebru-ethical-ai-jeff-dean-controversy-fired)
<br>
<br>
BBC News. (2021, February 20). Margaret Mitchell: Google fires AI ethics founder. BBC News. [link to article](https://www.bbc.com/news/technology-56135817)
<br>
<br>
Wakabayashi, D., & Metz, C. (2022, May 2). Another Firing Among Google’s A.I. Brain Trust, and More Discord. The New York Times. [link to article](https://www.nytimes.com/2022/05/02/technology/google-fires-ai-researchers.html)
