Readme MiniX2

lavet af Nadja G. Naneva, Æstetisk programmering 2023 semester 2

dette er linket til programmet: https://nadjananeva.gitlab.io/aep/MiniX2/index.html  

og efter efterspørgsel har jeg også tilføjet et link til min js fil: https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX2/Minix2.js 



For denne her aflevering har jeg lavet 2 emojis. Den ene skal repræsentere følelsen af at være overrasket og derfor er Emoji nr. 1 er et chokeret ansigt med en mund der udvider sig. Selve øjnene er ret små, fordi jeg mente det vil bringe en sødere udseende frem og farvevalg mæssigt har jeg valgt gul givet det er den er associeret med klassiske emojis. 


Emoji nr.2 er en mere romantisk emoji der blinker og hjerter omkring sig. Selve denne emoji var inspireret af den trend da folk selv sammensatte emojis. Det har jeg selv gjort og emoji 2 er min fortolkning af det.

Yderliger er grunden til jeg har valgt at lave emojierne på denne her måde er, fordi jeg ville se hvad der skete hvis man sammensatte animationer med emojis ligesom i gif for eksempel. Jeg havde svært ved at forestå variabler og mente dette ville være en god måde at øve dem på og lære variabler bedre at kende. Det blev også rådet til mig i den sidste MiniX i peer feedback området at udfordre mig selv på det område. Så dette er mit forsøg på at forklare de brugte variabler:

For emoji 1 er variablerne i brug disse:

let x = 0;


og 

//Emoji nr 1 mund

fill(255,200,100);
ellipse (140,310,x);
 
if (x<50){x++}
else if (x=50){x=0};

For at forklare skaber jeg med let x = 0; en variabel som jeg bruger nede i munden med en if statement. Hvad man kan se er der er givet en farve med fill kode, skabt en ellipse og dens koordinater samt diameter og efterfølgende if-statement. Hvad if-statementen siger er at hvis x er mindre en 50 skal den vokse og hvis den bliver 50 skal den sættes til 0.

Angående blinket med variablerne:

let wink = 0;

og

if (wink==10){noStroke()
wink=0}
else {stroke(0)}
wink++
console.log(wink);
curve(700,450,650,285,607,285,650,350);

kan vi se en skabelse (let wink = 0;) og en if-statement til. I denne if-statement står der at hvis wink er løst lig med 10 skal der ikke være noget omrids. Ved wink=0 giver vi wink en værdi og i else står der, der skal være et omkrids. Ligesom i x++ betyder wink++ at det skal vokse. cosole.log(wink); viser i din console winks værdi og selve curve funktionen er winket. 

Angående winket ville jeg ønske jeg vidste hvordan jeg gjorde sådan at ikke alle strokes var påvirket og forsvandt. Det er noget jeg gerne vil forbedre til næste gang.

Hvis man skulle se på emojis i den bredere sociale spectrum har vi læst, i litteraturen set en forelæsning og snakket i klassen om emojis betydning. Der er mange synsvinkler angående hvor mange emojis der skal være, hvilket farve de skal have og hvordan mennesker føler sig repræsenterede. Det er netop fordi vi som mennesker er forskellige der er så mange synsvinkler på emojis. Det er igennem den udvikling der er kommet de her problemstillinger op. Da emojis kun var tegn som denne XD eller denne :-P talte folk ikke om LGBT eller hudfarve i emojis. Personligt kan jeg godt forstå folks ønske om at blive repræsenteret og vil med glæde give dem det, men er nogle gange i tvivl hvordan jeg tager en stilling omkring mig selv uden at fornærme nogen. Der hvor jeg synes der mangler noget er flere følelser. Der er ofte ikke en emoji til at forklare hvad det er jeg føler, men det kan virke ret overfladisk. Der er her spørgsmålene fra undervisningen som hvad har man brug for at vise en følelse. Der det en hel krop som i kina med orz? Er et ansigt nok og hvad udgør et ansigt? Hvorfor har emojis fravalgt næsen? Og har emojis et politisk aspekt?



