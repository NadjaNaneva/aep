Readme Mini X 4

screenshot: 

![](https://NadjaNaneva.gitlab.io/aep/MiniX4/MiniX4billede.png)

link til miniX4: https://nadjananeva.gitlab.io/aep/MiniX4/index.html 

link til source code: https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX4/MiniX4.js

Beskrivelse af design:

I mit design har jeg lavet en skitse af en online shop som tilbyder forskellige tøj udvalg som kjoler, bluser, bukser/nederdele og endda mere niche ting som sko samt. smykker. Udover alle disse udvalgt er der en shopping kurv nede i højre hjørne for at efterligne den fysiske shoppinging oplevelse. Hvis du klikker på en af knapperne som “køb x vare” vil der poppe en notification op som sider “er nu tilføjet til kurv”. Den vil forsvinde hvis du klikker på “okay” knappen. Andre ting man kan se er nederst en video af dig selv- grunden til jeg har tilføjet det er fordi jeg blev inspireret af samtalerne der sker ofte i takt med emnet dataindsamling, som at vi bliver hele tiden lyttet på eller set på denne vores elektronik. Jeg vil gerne se hvordan det kan ændre en shopping oplevelse ved at man kan se sig selv shoppe. Vil man blive foragtet og stoppe? Vil man ignorere det eller finde måder at bruge det på som en slags model før man køber noget online? Det er det min Mini X4 gerne vil sætte spørgsmålstegn på.

Kode forklaring:

let knap;
 
//knap nederdel

  knap = createButton("køb nederdel");
  knap.position(485, 300);
  knap.size(80, 40);
  knap.style("background-color", "#00aaaa");
  knap.style("color", "#ffffff");
  knap.mousePressed(notification);

som kode gennnemgang har jeg valgt dette her eksempel. Det er en forklaring på hvorfor knappen under nederdel ser ud som den gør, er placeret hvor den er og hvorfor den kan hvad den kan. 

I kode eksemplet skaber vi en udefineret variabel som senere bliver til knappen “Køb nederdel”. Med knap.position vælger jeg hvor på canvaset det skal placeres og size bestemmer størrelsen. Under style bestemmer jeg farve på selve knappen og texten. Ved mousePressed siger jeg funktionen notification skal ske. Den ser sådan her ud: 

//få notification og okay knap synlig

function notification() {

  fill(0, 0, 0);

  knap = createButton("Notification: Er nu tilføjet til kurv");
  knap.position(350, 420);
  knap.size(230, 70);
  knap.style("background-color", "#823264");
  knap.style("color", "#ffffff");

  knap = createButton("Okay");
  knap.position(420, 510);
  knap.size(80, 40);
  knap.style("background-color", "#eb984e");
  knap.style("color", "#ffffff");
  knap.mousePressed(okay);

  storeItem (knap = createButton("Okay"));
  storeItem (knap = createButton("Notification: Er nu tilføjet til kurv"));

}

I denne funktion skaber jeg en notification og en okay knap som popper op hvis der er klikket på den. Hvad vi kan se her udover det allerede forklarede er at jeg bruger koden storeItem for at få den anden henviste funktion okay til at virke. Altså denne funktion:

//få notification og okay til at forsvinde

function okay() {

removeElements (knap);
video = createCapture(VIDEO);
video.size(800, 800);

}

hvad der sker her er vi får det hele til at gå tilbage til sådan som det var før, altså udgangspunktet.

Festivals ansøgning:

Dearest Transmediale Festival

My name is Nadezhda Georgieva Naneva and I would like to submit my programming project as an exhibition to your festival. I have been following your work through my university and have therefore taken an interest in making a contribution. The contribution I wish to make is a form of critical design on the topic of data gathering in our society. I have built a sketch of an online shop offering clothes with the possibility to see yourself in a web camera. I wish to spark a debate in regards to the data gathering for marketing purposes and if it in reality betters the user experience when they visually and more closely can feel the effects of the data they share.

I will be waiting for your reply
Best regards
Nadezhda Georgieva Naneva


Kulturelle implikationer af datafication og data capturing generelt

I klassen har vi haft mange interessante diskussioner omkring netop det emne. Det kan for mig ofte føles som et meget mørkt område hvor der mange gange mangler information. Jeg synes manglen på consent er problematisk - selv om vi har love om det i EU er det i mine øjne ikke nok og bekymrende når det er endnu værre i andre vidensområder.
