MiniX 5b readme  - MiniX5 version 2

Links til de forskellige steder og dele af mit program. 

screenshot: 

![](https://NadjaNaneva.gitlab.io/aep/MiniX5.b/MiniX5bbillede.png)

link til miniX5b: https://nadjananeva.gitlab.io/aep/MiniX5.b/index.html 

link til source code: https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX5.b/MiniX5b.js 

Hvad har jeg skabt?/ Beskrivelse af mit design: 

Siden jeg fejlede min første MiniX 5 har jeg lavet dette andet udkast inspireret af andres projekter og tekstilindustriens mønstre. Hvad man kan se i min nye Mini X5 (kaldt Mini X5b) er en lilla baggrund, to sorte streger der skaber et mønster og lyserøde cirkler der genereres tilfældigt på skærmen. Hvordan det sker vil jeg gerne forklare i det nedenstående kode-afsnit 

Kode forklaring:

let x = 50;
let y = 50;

Jeg starter ud med at skabe nogle variabler som skal være x og y koordinaterne i mit design. 

Angående function setup er det meget simpelt med at skabe en canvas og en baggrund med RGB værdier.

function setup () {
createCanvas(800, 800);
background(100,50,200);

}

Så begynder jeg på function draw. Der skaber jeg en lokal variabel som er r med værdien random (1). Jeg laver en if statement hvor der står at hvis r er mindre end 0.25 skal en linje tegnes mens hvis der er mindre en 0.5 skal en anden linje tegnes. I de linjer er det de variabler jeg har skabt hvilket får linjerne til at skabes der hvor de skabes. 

Uden for funktioner er det forklaret hvordan de rykker sig med x+=20 som får x til at stige og en ifstatemnet som holder de hele inden for canvaset. Det samme gælder for y som er nedenunder der, men her står der at loopet skal stoppes hvis det overskrider canvas.

For at gå tilbage til funktion draws if-statement står der at hvis r er mindre end 1 skal der ske et for loop. I det for loop skaber vi de cirkler der er med den farve de har. 

function draw () {

  let r = random(1);


  if (r < 0.25){
    stroke(random(0,0,0));
    line (x, y, x+30, y+30);
  } else if (r < 0.5){
    stroke(random(0,0,0));
    line (x, y+30, x+30, y);
  } else if (r < 1){

     for (let i = 0; i < 1; i++){
    
    fill(255, 192, 203, 150);
    ellipse(random(height), random(width), 75);

  }
  } 

x += 20;
if (x > width-70){
  x = 50;
  y += 20;
}

if (y>height-70){
  noLoop();
}
}


Refleksioner angående emnet, designet og regler i andet udkast

I min sidste Mini X 5 readme talte jeg om net.art generator og AI kunst-skabende værktøjer. Nu hvor jeg har også haft en fremlæggelse omkring der i Software Studies med min gruppe der og en klasse debat kan jeg fylde meget mere på der, men jeg har valgt ikke at gøre det for at ikke gøre det for ensrettet og fordi jeg endnu ikke har talt om de regler der er i mit design. 

Jeg har valgt at have to meget simple regler i mit design og det var at der skulle være to forskellige linjer i hver sin retning. Derudover ville jeg også gerne have de cirkler der var. Grunden til det er fordi det var et remix af andres projekter og inspireret af hvad de havde gjort samt timen med Anders Visti hvor vi fokuserede på netop det.

Jeg gik også efter denne effekt da jeg fandt specielt farverne æstetisk tiltrækkende. Det er et meget feminint design, hvilket appellerer til mig og mine ønsker.
Angående tekstilindustrien synes jeg det er meget interessant at se sådan nogle former i kodning. Nu hvor jeg også har haft muligheden for at se på fremlæggelserne synes jeg Lauras med strikning også tager det tema op og jeg kunne godt lide hun også havde prøvet det af selv. Jeg har blandt andet giver feedback til Sif som havde gået en hel anden vej, men jeg var stadigvæk helt imponeret at hendes produkt, ligesom Lisas med kunst for eksempel.. Andre der taget den tekstile industrivej er Jonas som jeg også har taget inspiration af angående mønster.
