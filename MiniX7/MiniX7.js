//skaber en variabel for knap
let knap;

//skaber variabler for x position

let x = 0;
let x1 = 0;
let xpos2 = 0;
let xpos3 = 0;
let xpos4 = 0;
let xpos5 = 0;
let xpos6 = 0;
let xpos7 = 0;
let xpos8 = 0;
let xpos9 = 0;
let xpos10 = 0;

//skaber variabler for y positioner

let y = 0;
let y1 = 0;
let y2 = 0;
let y3 = 0;
let y4 = 0;
let y5 = 0;
let y6 = 0;
let y7 = 0;
let y8 = 0;
let y9 = 0;


function setup() {
  // put setup code here

  //Laver en canvas og sætter en framerate

  createCanvas(700, 700);
  frameRate(6);

}

function draw() {
  // put drawing code here

  //skaber en baggrund for at 
  background(0, 50, 100);

  //difinerer xpositionen

  let xpos = random(width);
  let x1pos = random(width);
  let x2pos = random(width);
  let xpos3 = random(width);
  let xpos4 = random(width);
  let xpos5 = random(width);
  let xpos6 = random(width);
  let xpos7 = random(width);
  let xpos8 = random(width);
  let xpos9 = random(width);
  let xpos10 = random(width);

  //difinerer yposition

  let ypos = random(height);
  let y1pos = random(height);
  let y2pos = random(height);
  let y3pos = random(height);
  let y4pos = random(height);
  let y5pos = random(height);
  let y6pos = random(height);
  let y7pos = random(height);
  let y8pos = random(height);
  let y9pos = random(height);

  //skaber formene og farver dem ligesom i MiniX1, men ændre på værdierne

  fill(300, 100, 200);
  rect(xpos, ypos, x1pos, y1pos);

  fill(100, 200, 300);
  triangle(xpos, y2pos, x1pos, y3pos, x2pos, y4pos);

  fill(200, 300, 100);
  circle(xpos3, y5pos, xpos7);

  fill(300, 200, 100);
  square(xpos4, y6pos, xpos8);

  fill(200, 100, 300);
  ellipse(xpos5, y7pos, xpos9, y8pos);

  fill(100, 300, 200);
  circle(xpos6, y9pos, xpos10);

  //skaber en ellipse der skal følge mussen

  fill(255);
  ellipse(mouseX, mouseY, 20);

  //skaber en forloop som skriver hvor musen befinder sig i consollogen

  for (let i = 0; i < 10; i++) {
    console.log(mouseX)
    console.log(mouseY)
  }

  //skaber stop knappen

  let knap = createButton("Stop");
  knap.position(100, 400);
  knap.size(150, 70);
  knap.style("background-color", "#D35400");
  knap.mousePressed(noLoop);

  //skaber start knappen

  let knapstart = createButton("Start");
  knapstart.position(100, 200);
  knapstart.size(150, 70);
  knapstart.style("background-color", "#D35400");
  knapstart.mousePressed(loop);

}



