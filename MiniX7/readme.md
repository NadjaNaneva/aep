MiniX 7 readme  

Links til de forskellige steder og dele af mit program. 

screenshot: 

![](https://NadjaNaneva.gitlab.io/aep/MiniX7/MiniX7billede.png)

link til miniX5b: https://nadjananeva.gitlab.io/aep/MiniX7/index.html 

link til source code: https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX7/MiniX7.js 

Hvad har jeg skabt?/ Beskrivelse af mit design: 

I min Mini X7 har jeg taget udgangspunkt i min Mini X1 og har derudfra skabt en “opgraderet” version af mini X'en. Det en sammenblanding af datafication temaet, generativ system temaet (som jeg fejlede) og jeg havde egentligt ønsket også at tage fra objekt temaet, men det fungerede ikke. Det vil jeg forklare lidt mere om i kode afsnittet.

I min Mini X1 havde jeg skabt forskellige former og udforskede fill () koden, fordi det var det, der var svært, interessant og brugbart på det tidspunkt. Eksempel med min dengang fejlede color () kode. Jeg ville gerne lære andre også på det tidspunkt så tog mange af de tilbudte form koder fra p5.js referencelisten. 

Nu, i min Mini X7 har jeg remixet temaer ikke kun som et “bevis” på hvad jeg har lært, men mente det vil gøre det til et mindre fladt projekt - tværtimod skabende et et interaktivt system med en bruger.

Det gør jeg ved at skabe de to knapper man kan se - “start” og “stop”. Det er noget vi lærte fra datafication, men fra det tema har jeg også taget noget andet. Jeg har skabt en ellipse, der følger musen og har bedt om dens position bliver skrevet ind i console loggen. Det er også forklaret i kodeafsnittet. Jeg gjorde det, fordi jeg ikke brugte dem til min Mini X4 og gerne ville nu. Fortsat med beskrivelsen er der de samme figurer med samme farve, men inspireret fra generativness har jeg valgt at de skulle skifte størrelse og form

Ændringer:

Jeg har tilføjet 3 ting - to knapper, ændret på formen/størrelsen og en ellipse tilhæng musen. 

Grunden til det er kort beskrevet fordi jeg gerne ville gøre til et mere interaktivt program, øve mig på gamle koder og skabe noget kreativ ud fra noget “kedeligt”.

Kode forklaring:

Jeg sagde jeg ville tale om hvorfor jeg ikke fik objekt temaet med ind under. Jeg startede ud med at have planlagt de skulle ligesom player og kunne bevæges gennem piletasterne op og ned. Det fungerede ikke (min teori er på grund af position), men den ide blev nød til at blive scrappet.

Hvad der i stedet for blev lavet er en variabel for at skabe en knap

//skaber en variabel for knap
let knap;

og så knappen

  //skaber stop knappen

  let knap = createButton("Stop");
  knap.position(100, 400);
  knap.size(150, 70);
  knap.style("background-color", "#D35400");
  knap.mousePressed(noLoop);

det samme blev gjordt for den anden knap


  //skaber start knappen

  let knapstart = createButton("Start");
  knapstart.position(100, 200);
  knapstart.size(150, 70);
  knapstart.style("background-color", "#D35400");
  knapstart.mousePressed(loop);

}

for at få figurerne til at skifte position  skabte jeg nogle variabler for x positionerne

//skaber variabler for x position

let x = 0;
let x1 = 0;
let xpos2 = 0;
let xpos3 = 0;
let xpos4 = 0;
let xpos5 = 0;
let xpos6 = 0;
let xpos7 = 0;
let xpos8 = 0;
let xpos9 = 0;
let xpos10 = 0;

//difinerer xpositionen

  let xpos = random(width);
  let x1pos = random(width);
  let x2pos = random(width);
  let xpos3 = random(width);
  let xpos4 = random(width);
  let xpos5 = random(width);
  let xpos6 = random(width);
  let xpos7 = random(width);
  let xpos8 = random(width);
  let xpos9 = random(width);
  let xpos10 = random(width);

derefter giver jeg dem en værdi og indsætter dem ind i formene

 fill(300, 100, 200);
 rect(xpos, ypos, x1pos, y1pos);

det samme blev gjort for y, men jeg vil ikke bruge ord på det.

Senere skaber jeg en ellipse, der skal følge musen. Det gøre jeg med dette kodeafsnit:

 //skaber en ellipse der skal følge mussen

  fill(255);
  ellipse(mouseX, mouseY, 20);

  //skaber en forloop som skriver hvor musen befinder sig i consollogen

  for (let i = 0; i < 10; i++) {
    console.log(mouseX)
    console.log(mouseY)
  }

