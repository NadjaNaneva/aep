MiniX 6 readme

Links

screenshot: 

![](https://NadjaNaneva.gitlab.io/aep/MiniX6/MiniX6billede.png)

link til miniX4: https://nadjananeva.gitlab.io/aep/MiniX6/index.html 

link til source code: 

https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX6/miniX6.js

https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX6/Basket.js

https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX6/apple.js 


Hvad har jeg skabt?

I denne her omgang har jeg lavet et spil der går ud på fange æbler med en brun kurv. Det er inspireret af det gamle pengeby-spil og ligesom det falder der æbler ned som man skal fange. På selve siden kan man se en grøn baggrund, den brune kurv, 5 røde æbler og tekst. Den brune kurv kan gennem piletasterne rykkes fra den ene side til den anden. Angående teksten står der hvor mange æbler du har spildt og hvor mange du har spist. Hvis du ikke når at spise 3 i streg “taber” du.

Kode:

Angående kode har jeg tænkt mig at forklare nogle af funktionerne der får mit spil til at fungere og derudover bruge skabelsen af objektet “apple” som eksempel af et object. 

For at kunne forstå hvor nogle af værdierne kommer fra har jeg kopieret mine variabler ind som reference og  gøre det nemmere at læse. 

let apple = [];
let player;
let keyColor = 45;
let score = 0, lose = 0;
let antal = 5;

Den første funktion er denne her:

function checkApples() {
    if (apple.length < antal) {
        apple.push(new Apple());
    }
}

Hvad den gør at gennem et if-statement siger den at hvis arrayet med apple er mindre end variablen antal så skal en ny apple sættes ind. Det er sådan spillet kan fortsætte. 

Næste function er denne

function checkHarvest() {
    let distance;
    for (let i = 0; i < apple.length; i++) {
        distance = dist(player.posX, player.posY, apple[i].posX, apple[i].posY);

        if (distance < player.size / 3) {
            score++;
            apple.splice(i, 1);
        } else if (apple[i].posY < 3) {
            lose++;
            apple.splice(i, 1);
        }
    }
}

Her starter jeg ud med at skabe en variabel (distance) og definere den. Det matematikstykke der beregner afstanden mellem kurven og æblerne. I den if-statement der står der beskrives der at variablen score skal stige når æblet er “spist” og hvis der spildes skal det også stå der.
Det er denne her funktion det går at brugeren også kan se hvad deres score er på:

function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    text('You have eaten ' + score + " apple(s)", 10, height / 1.4);
    text('You have wasted ' + lose + " apple(s)", 10, height / 1.4 + 20);
    fill(keyColor, 255);
}

Men for at man også kan tabe spillet er der også denne her funktion som siger at hvis mere end 3 æbler i streg spilles skal der komme teksten “"Too Much WASTAGE...GAME OVER" op og loopet skal stoppes

function checkResult() {
    if (lose > score && lose > 2) {
        fill(keyColor, 255);
        textSize(26);
        text("Too Much WASTAGE...GAME OVER", width / 3, height / 1.4);
        noLoop();
    }



Til selve objekt eksemplet har vi det her eksempel.

Hvad vi starter ud med at skabe er en class og navngive den Apple. Hvad der står at der er en constructor som hjælper os med at bygge objektet og hvad det skal kunne med this.speed for eksempel. Senere kommer dens metoder på, også kendt i bogen som properties, som move og show som bestemmer hvad den skal kunne og hvordan den skal se ud.

class Apple {
    constructor() {
        this.posX = random(0, width);
        this.posY = random(0, height-80);
        this.speed = random(0.2, 2);
    }

    move(){
        this.posY += this.speed;
        if (this.posY > width){
            this.posY = 0;
        }
    }

    show() {
        fill(255, 0, 0);
        noStroke();
        ellipse(this.posX, this.posY, 50, 40);
        fill(0, 255, 0);
        ellipse(this.posX, this.posY - 25, 5, 20);
    }
}


Refleksion

Som mennesker undrer vi os tit om hvordan tingene omkring os fungerer, hvorfor de fungerer og hvordan de skal kunne fungere. I mine øjne er denne optimering en del af evolutionen der har ført menneskeligheden derhen hvor den er nu angående teknologi og industriel udvikling. Måske er der derfor vi også leder efter en forståelse i andre ting udover som fx. planter, dyr, liv i rummet eller bare objekter. Vi har sikkert sagt til højlydt til vores genstande når de ikke fungerede. 

Selvom mit spil er baseret på Pengeby, synes jeg også rent kulturelt den også taler om madspild lidt ligesom tofu.spillet i denne uges læsning gjorde.
