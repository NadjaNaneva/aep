class Player {
    constructor(){
        this.posX = width/2;
        this.posY = height-50;
        this.speed = 20;
        this.size = 50;
    }

    moveUp(){
        this.posX -= this.speed;
    }
    moveDown(){
        this.posX += this.speed;
    }

    show(){
        noStroke();
        fill(120, 70, 0);
        rect(this.posX, this.posY, this.size);
    }
}
