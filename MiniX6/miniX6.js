let apple = [];
let player;
let keyColor = 45;
let score = 0, lose = 0;

let antal = 5;

function setup() {
    createCanvas(650, 650);
    background(240);

    for (let i = 0; i < antal; i++) {
        apple.push(new Apple());
    }

    player = new Player();

}

function draw() {
    background(100, 180, 100);
    getApple();
    player.show();
    displayScore();
    checkResult();
    checkHarvest();
    checkApples(); 

}

function getApple() {

    for (let i = 0; i < apple.length; i++) {
        apple[i].move();
        apple[i].show();
    }
}

function checkApples() {
     if (apple.length < antal) {
     apple.push(new Apple());
     }
     }


function keyPressed() {
    if (keyCode === LEFT_ARROW) {
        player.moveUp();
    }
    if (keyCode === RIGHT_ARROW) {
        player.moveDown();
    }
    if (keyCode === ENTER) {
        setup();
        spawnApple();
        player.show();
    }
}

function checkHarvest() {
    let distance;
    for (let i = 0; i < apple.length; i++) {
        distance = dist(player.posX, player.posY, apple[i].posX, apple[i].posY);

        if (distance < player.size / 3) {
            score++;
            apple.splice(i, 1);
        } else if (apple[i].posY < 3) {
            lose++;
            apple.splice(i, 1);
        }
    }
}

function displayScore() {
    fill(keyColor, 160);
    textSize(17);
    text('You have eaten ' + score + " apple(s)", 10, height / 1.4);
    text('You have wasted ' + lose + " apple(s)", 10, height / 1.4 + 20);
    fill(keyColor, 255);
}

function checkResult() {
    if (lose > score && lose > 2) {
        fill(keyColor, 255);
        textSize(26);
        text("Too Much WASTAGE...GAME OVER", width / 3, height / 1.4);
        noLoop();
    }

}

