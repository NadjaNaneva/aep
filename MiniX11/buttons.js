///////////////////////
//PLAY BUTTON
function playButton(){
play = createButton('PLAY');
play.style("background-color", "Transparent");
play.style("border-width", "40px");
play.style("border-color", "#FFFFFF");
play.position(width / 2 - 120, height / 2 + 150);
play.style("font-size", "60px");
play.style("color", "Transparent");
play.style("border-radius: 15px 15px 15px 15px");

play.mouseOver(hover);
play.mouseOut(out);
play.mousePressed(playPressed);
}

////////////////////////////////////
//PLAY BUTTON FUNCTIONS
function playPressed() {
    removeElements();
    titleBgm.stop();
    click.play();
    webcamBgm.loop();
    sceneNum = 1;
}

//////////////////////////////
//YES BUTTON
function yesButton(){
    yes = createButton('YES');
    yes.style("background-color", "Transparent");
    yes.style("border-width", "15px");
    yes.style("border-color", "#FFFFFF");
    yes.position(width / 2-400, height / 2 + 145);
    yes.style("font-size", "40px");
    yes.style("color", "Transparent");
    yes.style("border-radius: 15px 15px 15px 15px");
    
    yes.mouseOver(hover);
    yes.mouseOut(out);
    yes.mousePressed(yesPressed);
}
///////////////////////////////
//YES BUTTON PRESSED
function yesPressed() {
    webcamBgm.stop();
    spookyBgm.loop();
    click.play();
    removeElements();
    sceneNum = 4;
    frameCount = 0;
}

///////////////////////////
//NO BUTTON
function noButton(){
    no = createButton('YES');
    no.style("background-color", "Transparent");
    no.style("border-width", "15px");
    no.style("border-color", "#FF0000");
    no.position(width / 2+200, height / 2+ 145);
    no.style("font-size", "40px");
    no.style("color", "Transparent");
    no.style("border-radius: 15px 15px 15px 15px");
    
    no.mouseOver(noHover);
    no.mouseOut(noOut);
    no.mousePressed(noPressed);
}

/////////////////////////////////////
//NO BUTTON PRESSED
function noPressed() {
    webcamBgm.stop();
    spookyBgm.loop();
    click.play();
    removeElements();
    sceneNum = 3;
    input = createInput(""); //Creates a input function to be loaded in once
    input.position(width / 2 - 100, height / 2 + 100);
    submitButton();
}




////////////////////////////
//SUBMIT BUTTON
function submitButton(){
    submit = createButton("SUBMIT");
    submit.position(width / 2 - 150, height / 2 + 150);
    submit.mousePressed(submitPressed);
    submit.style("background-color", "Transparent");
    submit.style("border-width", "20px");
    submit.style("border-color", "#FF0000");
    submit.style("font-size", "60px");
    submit.style("color", "Transparent");
    submit.style("border-radius: 15px 15px 15px 15px");

    submit.mouseOver(hover);
    submit.mouseOut(out);
}
///////////////////////
//SUBMIT FUNCTIONS
function submitPressed() {
    click.play();
    removeElements();
    sceneNum = 4;
    frameCount = 0;
    inputValue = true; //Stops the 
    loadingBar = 0; //Ensures loadingBar can be run again!
}


//ALL BUTTONS HOVER AND OUT
function hover() {
    play.style("border-color", "#F81894");
    yes.style("border-color", "#F81894");
    submit.style("border-color", "#460000");
}
function out() {
    play.style("border-color", "#FFFFFF");
    yes.style("border-color", "#FFFFFF");
    submit.style("border-color", "#FF0000");
}

function noHover(){
    no.style("border-color", "#460000");
}
function noOut(){
    no.style("border-color", "#FF0000");
}
