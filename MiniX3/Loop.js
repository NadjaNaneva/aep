
let angle = 0;


function setup() {
  // put setup code here
  createCanvas(800, 800);
  angleMode(DEGREES);
  frameRate(6);

}

function draw() {
  // put drawing code here 

  background(0, 0, 0, 60);

  translate(width / 2, height / 2);


  //Få solsystemet til at bevæge sig
  rotate(angle);
  angle = angle + 40;
  console.log(angle);

  //sol
  fill(255, 150, 60);
  circle(0, 0, 100);

  //Jorden - havet
  fill(65, 65, 200);
  circle(200, 0, 40);

  //Jorden - land
  fill(0, 255, 0);
  noStroke();
  circle(200, -10, 10);
  circle(202, -4, 10);
  circle(205, -9, 10);
  circle(197, 12, 10);
  circle(200, 11.5, 10);
  circle(190, 2.5, 6);
  circle(213, 5, 3);






}
