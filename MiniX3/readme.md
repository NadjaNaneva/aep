Mini X 3 readme:

![](https://NadjaNaneva.gitlab.io/aep/MiniX3/MiniX3billede.png)

Link til MiniX 3: https://nadjananeva.gitlab.io/aep/MiniX3/index.html 

Link til js filen: https://gitlab.com/NadjaNaneva/aep/-/blob/main/MiniX3/Loop.js 

I denne her omgang har jeg lavet en throbber med to cirkler - en gul og en blå med grønne pletter. Den gule cirkel skal forestille at være vores velkendte sol, mens den blå-grønne cirklen skal forestille jorden og dens kredsløb i universet (det er derfor baggrunden er sort)

Hvorfor så lige det her? Selve opgaven går ud på at udforske loops, så hvorfor ikke den loop der har og gør det her muligt? Software kulturen og teknologien ville ikke eksistere (ligesom mange andre ting) uden os. På den anden side kan man argumentere for de store samfundsmæssige bidrag teknologi har tilbudt og har derfor også på en måde været med til at forme menneskeligheden som vi kender den idag. En throbber er fortolker vi som et symbol på maskinen tænker. Hvad det egentlig betyder er der er en buffering og er et ikon for det. Fx. når man åbner en meget stor datafil. 

En anden grund til jeg valgte jorden + solen som symbol for throbber er fordi det illustrere den største foreskel mellem menneskers tid og computerens tid. Denne forskel er også noget vi har læst om i teksten Hans Lammerant, “How humans and machines negotiate experience of time,” in The Techno-Galactic Guide to Software Observation. Hvad vi får at vide der er hvordan menneske tid er bestemt af ting som levetid, døgnet og lign., mens computeren befinder sig i sin egen lille tidskapsel - altså før vi kunne koble os på internettet eller opdateringer.

Andre ting fra uges materialle jeg har brug for at skabe min throbber er nogle af de nyt introducerede koder som translate() og rotate().

I koden kan man finde dem fx. i denne her linje:

translate(width / 2, height / 2);

som betyder at jeg gerne vil have rykke startspunktet fra (0,0) til halvden af canvases bredde og højde hvilket svarer til centrum.

Angående rotate() bruger jeg den til at få solsystemet til at bevæge sig. (Ja, selv solen men fordi det er en cirkel kan man ikke se det)

Det sker i det her kode afsnit her:

let angle = 0;
(...)
angleMode(DEGREES);
(...)
//Få solsystemet til at bevæge sig
rotate(angle);
angle = angle + 40;
console.log(angle);

For at forklare betyder let angle = 0; at jeg skaber en variabel der hedder angle og har talværdien nul. Så springer jeg nogle linjer for at vise hvad der er relevant med angleMode(DEGREES);. Hvad denne funktion gør er at den ændrer er måden vi beregner vinkler på. Vores computer bruger radians til at beregne vinkler, men det gør vi mennesker ikke, så for at gøre det nemmere for mig selv har jeg lavet denne ændring. Så kommer der en kommatering for at forklare hvad der er jeg skaber. Derefter kommer rotate koden hvor jeg siger jeg gerne vil roter hvad der følger efter med min variabel angle. Angle har jo værdien 0 hvilket vi ikke få jordkloden til at rykke sig, men koden angle = angle + 40; gør at variablen vokser og derfor rykker sig. Til sidst sider jeg, jeg gerne vil skrive angles værdi i console logen med koden console.log(angle);

Hvis jeg skal tænke over hvordan vi møder throbberen i digital kultur ville jeg nævne et eksempel med hvordan bestemte internetsider tager mere tid at loade end andre eller ikke vil loade hvis man ikke har internet. Hvis vi ændre tanken fra at en throbber er et tænke symbol ville jeg tænke på det som en loop. Hvis jeg ser på den bliver jeg tit svimmel og det kan virke hypnotiserende. I undervisningen har vi talt om hvordan vi oplever loops - selv fra små ting som boomerang funktionen i Instagram stories til den massive loop omkring dagen som jeg har taget udgangspunkt i. Loops i menneskehjernen skaber en rutine - hvad en ens holdning til det er. Ansvar som det at tage i skole/arbejde./lign. hver dag en også en form for loop. Throbberen er programmeret som en loop, og vi har også to typer (for og while) loops i programering, men det er interessant hvorfor vi så ikke fortolker den sådan. Vi bliver irriteret, måske fordi vi er vandt til at få ting med det samme eller bare ikke ved hvad vi skal gøre mens vi venter eller mangler viden til at hjælpe computeren med problemet. Jeg overvejede at lave det som en if mouse pressed () spil for at gøre det sjovere, men følte det var for urealistisk iforhold til der rigtige kredsløb som jeg forsøgte at illustrere. Jeg overvejer om det at gøre throbberen unik hver gang eller give den mere farve vil hjælpe?
